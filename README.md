# gorilla-new

`gorilla-new` is a script to create a Clojure project that uses Gorilla REPL. The script ensures the necessary dependencies, creates the project, and launches a Gorilla REPL within that project.

## Installation

```bash
$ git clone https://gitlab.com/jasonmm/gorilla-new.git
$ cd gorilla-new
$ chmod a+x gorilla-new.sh
$ cp gorilla-new.sh /usr/local/bin/
```

## Usage

```bash
$ gorilla-new.sh <project name>
```

The script will install Clojure and/or Leiningen if they are not installed. It will then create a Clojure project named `<project name>` using `lein new app <project-name>` and, finally run `lein gorilla` to start the Gorilla REPL.

#### Example

```bash
$ gorilla-new.sh world-changing
Clojure found.
Leiningen found.
Creating new project
Generating a project called world-changing based on the 'app' template.
Starting Gorilla REPL
Gorilla-REPL: 0.6.0
Unable to reach update server.
Started nREPL server on port 57863
Running at http://127.0.0.1:57865/worksheet.html .
Ctrl+C to exit.
``` 