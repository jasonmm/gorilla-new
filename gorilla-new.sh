#!/bin/sh
#
# This script creates a new Clojure project that includes the Gorilla REPL
# and starts the Gorilla REPL.
#
# Usage: $ gorilla-new.sh <project-name>
#

InstallClojureOnLinux() {
    if [ "$(command -v clojure)" ]; then
        echo "Clojure found";
    else
        if [ "$(command -v curl)" ]; then
            echo "Installing Clojure...";
            curl -O https://download.clojure.org/install/linux-install-1.10.1.483.sh
            chmod +x linux-install-1.10.1.483.sh
            sudo ./linux-install-1.10.1.483.sh
        else
            echo "curl not found.";
            echo "curl must be installed in order to install Clojure on Linux.";
            echo "See https://www.clojure.org/guides/getting_started#_installation_on_linux";
        fi
    fi
}

InstallClojureOnMac() {
    if [ "$(command -v clojure)" ]; then
        echo "Clojure found.";
    else
        if [ "$(command -v brew)" ]; then
            echo "Installing Clojure...";
            brew install clojure
        else
            echo "Homebrew not found.";
            echo "Homebrew must be installed in order to install Clojure on MacOS.";
            echo "See https://www.clojure.org/guides/getting_started#_installation_on_mac_via_homebrew";
        fi
    fi
}

InstallLeiningen() {
    if [ "$(command -v curl)" ]; then
        echo "Installing Leiningen...";
        curl https://raw.githubusercontent.com/technomancy/leiningen/stable/bin/lein > lein
        sudo mv lein /usr/local/bin/lein
        sudo chmod a+x /usr/local/bin/lein
        lein
    else
        echo "curl not found.";
        echo "curl must be installed in order to install Leiningen.";
    fi
}

# Check for Java
if [ ! "$(command -v java)" ]; then
    echo "Java not found.";
    echo "Java is required. Please install Java and then re-run this script.";
    exit 10;
fi

# Ensure Clojure is installed
case $(uname -s) in
    Linux*) InstallClojureOnLinux;;
    Darwin*) InstallClojureOnMac;;
esac

# Ensure Leiningen is installed
if [ ! "$(command -v lein)" ]; then
  InstallLeiningen;
else
  echo "Leiningen found.";
fi

PROJECTNAME=$1
CLOJUREVERSION="$(clojure -e '(clojure-version)')"

echo "Creating new project"
lein new app "$PROJECTNAME"
cd "$PROJECTNAME" || exit 11;

cat << EOF > project.clj
(defproject $PROJECTNAME "0.1.0-SNAPSHOT"
  :dependencies [[org.clojure/clojure $CLOJUREVERSION]]
  :main ^:skip-aot $PROJECTNAME.core
  :target-path "target/%s"
  :plugins [[org.clojars.benfb/lein-gorilla "0.6.0"]]
  :profiles {:uberjar {:aot :all}})
EOF

echo "Starting Gorilla REPL"
lein gorilla
